<?php

namespace Zarchp\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zarchp\Models\User;

class UserMutator
{
    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param array $args The arguments that were passed into the field.
     * @param GraphQLContext|null $context Arbitrary data that is shared between all fields of a single query.
     * @param ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function create($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        if (isset($args['images'])) {
            // todo: upload image
        } else {
            $image_name = date('YmdHis') . str_random(8) . '.png';
            $image_path = 'storage/images/users/';
            $image = $image_path . $image_name;
            \Avatar::create($args['name'])->save(public_path($image), 100);
            $args['image'] = asset($image);
        }

        return User::create($args);
    }

    public function update($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        unset($args['directive']);
        $update = User::where('id', $args['id'])
            ->update($args);

        return User::find($args['id']);
    }
}
