<?php

use Faker\Generator as Faker;
use Zarchp\Models\Post;
use Zarchp\Models\User;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return User::all()->random()->id;
        },
        'title' => $faker->sentence,
        'content' => $faker->paragraphs(3, true),
        'image' => $faker->imageUrl()
    ];
});
