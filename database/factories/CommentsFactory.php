<?php

use Faker\Generator as Faker;
use Zarchp\Models\Comment;
use Zarchp\Models\Post;
use Zarchp\Models\User;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'post_id' => function () {
            return Post::all()->random()->id;
        },
        'user_id' => function () {
            return User::all()->random()->id;
        },
        'name' => $faker->name,
        'message' => $faker->sentences(2, true)
    ];
});
