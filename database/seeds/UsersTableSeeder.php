<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use Illuminate\Database\Seeder;
use Zarchp\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = 'Anzar Syahid';
        $image_name = date('YmdHis') . str_random(8) . '.png';
        $image_path = 'storage/images/users/';
        $image = $image_path . $image_name;
        Avatar::create($name)->save(public_path($image), 100);
        User::create([
            'name' => $name,
            'email' => 'zarchp@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('123qweasd'), // 123qweasd
            'remember_token' => str_random(10),
            'image' => asset($image)
        ]);

        factory(User::class, 10)->create();
    }
}
